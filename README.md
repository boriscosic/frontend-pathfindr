# PathFindr

This is a react/redux game for a programming assignment. Once the game is running it can be accessed from the browser on **http://localhost:8000**. A path will be shown for a period of time. The path will then disappear and the goal of the game is to trace the path by clicking on each tile. If the path has been successfully completed the game will advanced to the next level with increased number of tiles and reduced time to observe the path. If at any point an incorrect tile in the path is clicked the game will end and prompt the user to enter his or her name into the hall of fame. The hall of fame features top ten entries sorted by highest step reached and percentage or last step completed (note: the app utilizes local storage for all data persistence).

Problem: https://bitbucket.org/adigsd/frontend-pathfindr

## Requirements

The app used nodejs for building and managing dependencies. The recommended path of installing nodejs is via [nvm](https://github.com/creationix/nvm). The other alternative provided within this repo is running the package via Docker. After the packages are installed the following should be on the system:

- node ^6.0
- npm ^3.0

## Getting Started

### Docker

This document assumes Docker is installed and running on the machine. Docker can be installed on a number of platforms by following [instructions](https://docs.docker.com/engine/installation/) on their websites. Before running Docker, ensure the folder `node_modules` is removed. `node_modules` are not necessarily universally compatible and certain tool or utilities may have platform specific requirements.

```
$ docker-compose build
$ docker-compose up
```

This will take a minute to build the image which when ready will display following:
```
Child html-webpack-plugin for "index.html":
    chunk    {0} index.html 533 kB [rendered]
        [0] ./~/html-webpack-plugin/lib/loader.js!./app/src/static/index.ejs 591 bytes {0} [built]
        [1] ./~/lodash/lodash.js 532 kB {0} [built]
        [2] (webpack)/buildin/module.js 251 bytes {0} [built]
webpack: bundle is now VALID.
```
If the output is similar to the one above, the app is ready to be accessed from **http://[docker-machine-ip]:8000**. To find out docker machine IP type `docker-machine ip` in another terminal.

### Locally

After confirming the requirements for development, follow the steps below to get underway:

```
$ git clone https://bitbucket.org/boriscosic/frontend-pathfindr
$ cd frontend-pathfindr
$ nvm use
$ nvm install
$ npm install
$ npm start

```
This should generate the following output:
```
Child html-webpack-plugin for "index.html":
    chunk    {0} index.html 533 kB [rendered]
        [0] ./~/html-webpack-plugin/lib/loader.js!./app/src/static/index.ejs 591 bytes {0} [built]
        [1] ./~/lodash/lodash.js 532 kB {0} [built]
        [2] (webpack)/buildin/module.js 251 bytes {0} [built]
webpack: bundle is now VALID.
```
If the end output is similar to the one above the app is ready to be accessed from **http://localhost:8000**. Ensure that the `app/dist` folder does not contain any output files. Webpack dev server will serve files found in `app/dist` rather then generate new ones via proxy as desired.

### Contributing

Before making contributions it is recommended to connect to pre-commit hook to test and lint the application:

```
ln -s "$(pwd)/hooks/pre-commit.sh" .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```

These steps can also be run manually:

```
$ npm test
$ npm run lint
```

### Application structure

```
.
├── app                          # Application source code
│	├── dist                     # Placeholder for build and release folder
│	├── src                      # Application source code
│       ├── components           # React components
│           └── Tile             # Bootstrap main application routes with store
│               ├── Tile.jsx     # JSX component
│               ├── Tile.scss    # Style for the component
│               ├── Tile.spec.js # Unit test for the component
│               └── ...          
│       ├── ...              
│   ├── layouts                  # React page structure container
│   ├── containers               # React containers
│   ├── lib                      # Shared libraries
│   ├── modules                  # Reducers and actions
│   │   ├── play.js              # Action and reducer
│   │   ├── play.spec.js         # Test for action and reducer
│   │   ├── ...
│   ├── static                   # Static page assets
│   └── store                    # Redux-specific pieces
│       ├── createStore.js       # Create and instrument redux store
│       ├── routes.js            # React/redux router
│       └── reducers.js          # Reducer registry and injection
├── test                         # Acceptance tests
├── hooks                        # Git hooks for pre-commit hooks
├── package.json                 # Build config for npm
├── webpack.config.js            # Build config for webpack
├── ...                          # Miscellaneous config files for various tools
```

### Deploying

During development Webpack will use live reloading for faster development. In order to build a production ready app run:

```
$ npm run build
```
Distribution files can be found in `app/dist`. There is no need to add absolute paths to index.ejs. All linking is done dynamically.

## Tests

This app has a series of unit test and acceptance tests.

### Unit Tests

The unit tests were created using [Mocha](https://mochajs.org/), [Chai](http://chaijs.com/), [Sinon](http://sinonjs.org/), and [enzyme]('https://github.com/airbnb/enzyme'). They test all react components, actions, reducers and libs. Tests can be found in component folders under `*.spec.js`.

To run the unit tests simply run `npm test`.

### Acceptance Tests

Acceptance tests were created, in addition to unit test, in order to verify the application from the user end point. They launch a Selenium Chrome instance and perform a test through all game steps. Acceptance tests were created using [Webdriver.IO](http://webdriver.io/), [Mocha](https://mochajs.org/) and [Chai](http://chaijs.com/). In order to run acceptance tests Java is required on the machine. Java can be installed via `brew cask install java` on OSX.

Following Java installation open another terminal and run `selenium-standalone install` followed by `selenium-standalone start`. If selenium listener starts without any issues, go to the root folder in a *different window or terminal* of frontend-pathfindr and run `run test:client`.

[Click here](http://pathfindr.seeboriscode.com/pathfindr-demo.mov) for demo of program testing a program.

## Appendix

### Why React/Redux?

1. React represent components as files and each component belongs to one file. The components are updated by a watching a state and state can be updated by dispatching an action. The Single Responsibility Principle implied through React architecture ensures components are small, reusable and most importantly east to test.  

2. Having had experience with frontend frameworks such as, Angular, Backbone and Ember, there is something to be said about their advanced learning curve. The API documentation exposes a number of new tags that are framework specific. React/Redux code is JavaScript (with exception of JSX) and embraces the latest development in the JavaScript community.

3. React works well along ES6, Babel and Weback. ES6 is a great leap forward for the future of JavaScript and Webpack brings along quality tools for creating packages and working within that environment.

4. Healthy ecosystem of independent libraries ensures that the final application is not bound to a particular vendor or pattern. React is the base framework on which any number of libraries can be added as required. With ES6 and Webpack it is now possible to import partial or complete libraries with their dependencies handled automatically.

5. Applications can be refactored with features or bug fixes in true Agile format. React components are state dependent and incremental changes to a component are less likely to affect other components relying on that state. Furthermore, Redux manages each state in an isolated module sandbox thus ensuring the components are updating only when they require data from a particular state object.

6. When combined with Redux, React can dispatch actions to reducers which in turn update state. This means, that no particular component needs to know what other portions of the applications are doing. Once a component is subscribed to an object in the state it will update only when that state object updates.

### What about layouts and responsive grids?

Through previous projects I have found that Bootstrap and Foundation can come with heavy dependency footprints. As an advocate of Bootstrap, I include it on projects where its UI capabilities can be showcased. For single page applications the view can usually be bound to a state model and the majority of JavaScript becomes unnecessary. The same statement can be said for Bootstrap DOM components. Tabbed contents, panels or modals can come with complex DOM layouts that make readability and maintainability difficult. Currently I am in favour of [csswizardry-grids](https://github.com/csswizardry/csswizardry-grids) or [flexbox](https://github.com/philipwalton/solved-by-flexbox).

### What other framework are in here?

* lodash: support for assign, and fill which are not part of IE11. Using ES6 it is possible to import submodules required from this library.
* mocha/chai: for setting up test cases.
* babel: ES6 support.
* async: allow acceptance test to be run in synchronous order.
* eslint: detect code smells using airbnb lint file.
* sass: css compiler.
* webdriver.io/selenium: for browser simulation and setting up acceptance test cases.

### How does the build system work?

It's all done via `npm` and `webpack`. I find the less package management systems are involved the easier it is to optimize and develop.

### How do I make the game harder?

The number of steps is injected globally via `webpack.config.js`. Look for `GAME_STEPS` variable and set it to another number. `STARTING_TILES_PER_ROW` is used to initialize the board at Step 0. Webpack should be restarted (Ctrl + C && node start) in order for these variables to take effect.

### Demo?

If there are any issues with the build, contact me at boriscosic@gmail.com or checkout http://pathfindr.seeboriscode.com/.
