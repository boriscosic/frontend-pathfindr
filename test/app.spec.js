/* eslint-disable */
import { assert } from 'chai';
import async from 'async';

describe('client test', function() {
    it('user can finish the game', function(done) {
        this.timeout(150000);
        browser.url('http://localhost:8000');
        assert.equal(browser.getTitle(), 'PathFindr');

        browser.waitForVisible('#board', 2000);
        let testSeries = [];
        let steps = parseInt(browser.getAttribute('#board', 'data-steps'));

        // synchronously test the game dynamically based on steps
        Array(steps).fill(0).forEach((val, step) => {
            const tiles = Math.pow(step + 4, 2);

            testSeries.push(
                (cb) => {
                    browser.waitForVisible(`.board-step-${step}-ready`, 10000);
                    assert.equal(browser.elements('.tile').value.length, tiles);
                    cb();
                }
            );

            testSeries.push(
                (cb) => {
                    for (let i = 0; i < tiles; i++) {
                        if (browser.isExisting(`.tile-${i}`)) {
                            browser.click(`.tile-${i}`);
                            browser.pause(500);
                        }
                    }
                    cb();
                }
            );
        });

        // test the end form and local storage
        testSeries.push(
            (cb) => {
                browser.waitForVisible('.score', 1000);
                browser.setValue('#yourName', 'test user');
                browser.click('#saveHighScore');
                browser.waitForVisible(`.list`, 1000);
                assert.notEqual(browser.getText('.list').indexOf('test user'), -1);
                assert.notEqual(browser.localStorageSize(), 0);

                browser.localStorage('GET', 'PathFindrScore', (res) => {
                    assert.deepEqual(JSON.parse(res), [{ player: 'test user', step: 'steps', percentage: '100' }]);
                    cb();
                });
            }
        );
        async.series(testSeries);
    });
});
