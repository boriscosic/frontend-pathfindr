import fill from 'lodash/fill';

export default class Board {
    static generate(tileCount) {
        const items = fill(Array(Math.pow(tileCount, 2))).map((val, index) => ({
            playable: false,
            played: false,
            visible: true,
            error: false,
            order: -1,
            index
        }));

        const minMax = (row) => {
            const index = Math.pow(tileCount, 2) - (tileCount * row);
            const min = index - tileCount + 1; // eslint-disable-line
            const max = index - 2;
            return { min, max };
        };

        let last = null;
        let order = 0;

        for (let i = 0; i < tileCount; i += 1) {
            let bounds = minMax(i);
            last = last === null ? this.getRandomInt(bounds.min, bounds.max) : last;

            if (i === 0) {
                items[last] = { ...items[last], playable: true, order };
            } else {
                const tiles = (i === 1) ? 1 : this.getRandomInt(2, 4);
                for (let j = 0; j < tiles; j += 1) {
                    last -= tileCount;
                    if (last > 0) {
                        items[last] = { ...items[last], playable: true, order: order += 1 };
                    }
                }

                i += tiles - 1;
                bounds = minMax(i);
                let horizontal = 1;
                if (last - bounds.min > bounds.max - last) horizontal = -1;

                do {
                    last += horizontal;
                    if (last > 0 && items.filter((e, index) => e.playable && index < tileCount - 1).length === 0) {
                        items[last] = { ...items[last], playable: true, order: order += 1 };
                    }
                } while (last < bounds.max && last > bounds.min && this.getRandomInt(0, 10) % 2 !== 0);
            }
        }

        return items;
    }

    static isValid(index, lastTile, tiles) {
        let lastIndex = -1;
        let nextIndex = -1;
        const sortedPath = Object.assign([], tiles);

        sortedPath.sort((a, b) => (a.order < b.order) ? 1 : ((b.order < a.order) ? -1 : 0)); // eslint-disable-line
        tiles.forEach(t => (lastIndex = t.playable ? t.index : lastIndex));
        sortedPath.forEach(t => (nextIndex = t.playable && !t.played ? t.index : nextIndex));

        if (!tiles[index].playable) return false;
        else if (lastTile === null && index !== lastIndex) return false;
        else if (nextIndex !== index) return false;
        return true;
    }

    static getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min; // eslint-disable-line
    }
}
