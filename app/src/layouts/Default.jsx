import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

const Default = props => (
    <div>
        {props.children}
    </div>
);

Default.propTypes = {
    children: PropTypes.object // eslint-disable-line
};

export default connect()(Default);
