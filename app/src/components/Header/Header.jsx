import React, { PropTypes } from 'react';
import './Header.scss';
import { STATUS_LOOSE, STATUS_WIN } from '../../modules/play';

const Header = props => (
    <header>
        <h1>{'PathFindr'}</h1>
        <h2>
            Step {props.step}

            {props.status === STATUS_LOOSE && `: ${props.percentage}% - Game Over`}
            {props.status === STATUS_WIN && `: ${props.percentage}% - You Win!`}
        </h2>
    </header>
);

Header.propTypes = {
    percentage: PropTypes.number,
    status: PropTypes.string,
    step: PropTypes.number
};

export default Header;
