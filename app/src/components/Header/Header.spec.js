/* eslint-disable react/jsx-filename-extension */
import { assert } from 'chai';
import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('<Header />', () => {
    it('component should contain step 0', () => {
        const wrapper = shallow(<Header status={'STATUS_NEW'} step={0} percentage={0} />);
        assert.equal(wrapper.find('h2').length, 1);
        assert.equal(wrapper.find('h2').text(), 'Step 0');
    });

    it('component should show on game over', () => {
        const wrapper = shallow(<Header status={'STATUS_LOOSE'} step={5} percentage={20} />);
        assert.equal(wrapper.find('h2').length, 1);
        assert.equal(wrapper.find('h2').text(), 'Step 5: 20% - Game Over');
    });

    it('component should show on game win', () => {
        const wrapper = shallow(<Header status={'STATUS_WIN'} step={10} percentage={100} />);
        assert.equal(wrapper.find('h2').length, 1);
        assert.equal(wrapper.find('h2').text(), 'Step 10: 100% - You Win!');
    });
});
