/* eslint-disable react/jsx-filename-extension */
import { assert } from 'chai';
import React from 'react';
import { shallow } from 'enzyme';
import Board from './Board';
import Tile from '../Tile/Tile';

describe('<Board />', () => {
    before(() => {
        global.GAME_STEPS = 10;
        global.STARTING_TILES_PER_ROW = 2;
    });

    it('component should contain 9 tiles', () => {
        const tiles = [
            { playable: true, played: false },
            { playable: false },
            { playable: false },
            { playable: false },
            { playable: false },
            { playable: false },
            { playable: false },
            { playable: false },
            { playable: false }
        ];
        const wrapper = shallow(<Board color={'#ff0000'} tiles={tiles} size={500} />);
        assert.equal(wrapper.find(Tile).length, 9);
    });
});
