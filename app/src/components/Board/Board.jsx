import React, { PropTypes } from 'react';
import Tile from '../Tile/Tile';
import { STATUS_PLAYING, STATUS_NEW } from '../../modules/play';
import './Board.scss';

const Board = props => (
    <div
        className={`board board-step-${props.step}-${props.status === STATUS_PLAYING ? 'ready' : 'waiting'}`}
        id='board' data-steps={GAME_STEPS}
    >
        { props.tiles.map((tile, index) =>
            <Tile
                dimensions={parseInt(Math.sqrt(props.size / props.tiles.length), 0)}
                error={tile.error}
                index={index}
                key={`tile_${index}`}
                onTileClick={props.onTileClick}
                order={tile.order}
                playable={tile.playable}
                played={tile.played}
                visible={tile.visible}
            />)
        }

        { props.status === STATUS_PLAYING &&
            <p className='help-go'><strong>Go!</strong></p>
        }

        { props.status === STATUS_NEW &&
            <p className='help-go'>... wait for <strong>go</strong> ...</p>
        }

        <p className='help-block'>
            {'Tiles will display for a set period of time and then disappear.'}<br />
            {'Starting from the bottom, click on tiles representing the path.'}
        </p>
    </div>
);

Board.propTypes = {
    onTileClick: PropTypes.func,
    status: PropTypes.string,
    step: PropTypes.number,
    tiles: React.PropTypes.arrayOf(React.PropTypes.object)
};

export default Board;
