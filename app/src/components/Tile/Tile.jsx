import React, { PropTypes } from 'react';
import './Tile.scss';

const Tile = props => (
    <button
        className={`tile tile-${props.order} tile-${props.playable ? 'playable' : 'disabled'}` +
            ` ${props.played ? 'tile-played' : ''}` +
            ` tile-${props.visible ? 'visible' : 'hidden'}` +
            ` ${props.error ? 'tile-error' : ''}`}
        onClick={props.onTileClick.bind(this, props.index)}
        style={{
            height: `${props.dimensions}px`,
            width: `${props.dimensions}px`
        }}
    />
);

Tile.propTypes = {
    dimensions: PropTypes.number,
    error: PropTypes.bool,
    index: PropTypes.number,
    onTileClick: PropTypes.func,
    order: PropTypes.number,
    playable: PropTypes.bool,
    played: PropTypes.bool,
    visible: PropTypes.bool
};

export default Tile;
