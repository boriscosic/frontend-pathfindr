import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchTiles, hideTiles, toggleTile, setStatus,
    STATUS_LOOSE, STATUS_WIN, STATUS_NEW, STATUS_END } from '../modules/play';
import { setHighScore, saveHighScore, enableScoreForm } from '../modules/score';
import Board from '../components/Board/Board';
import Header from '../components/Header/Header';
import Score from '../components/Score/Score';

class GameScreen extends Component {
    componentDidMount() {
        this.props.fetchTiles();
        this.size = Math.pow(document.getElementById('board').offsetWidth, 2);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.status === STATUS_NEW) {
            setTimeout(() => this.props.hideTiles(),
                (5000 - this.props.step * 500)); // eslint-disable-line
        } else if (nextProps.status === STATUS_END) {
            setTimeout(() => this.props.setStatusLoose(), 1000);
        }
    }

    render() {
        let screenDisplay =
            (<Board
                tiles={this.props.tiles}
                size={this.size}
                onTileClick={this.props.onTileClick}
                status={this.props.status}
                step={this.props.step}
            />);

        if ([STATUS_LOOSE, STATUS_WIN].indexOf(this.props.status) > -1) {
            screenDisplay = (
                <Score
                    form={this.props.form}
                    list={this.props.list}
                    onHighScoreChange={this.props.onHighScoreChange}
                    onPlayClick={this.props.fetchTiles}
                    onSaveClick={this.props.onSaveClick.bind(this, this.props.step, this.props.percentage)}
                    player={this.props.player}
                />);
        }

        return (
            <div>
                <Header
                    percentage={this.props.percentage}
                    status={this.props.status}
                    step={this.props.step}
                />
                {screenDisplay}
            </div>
        );
    }
}

GameScreen.propTypes = {
    fetchTiles: PropTypes.func,
    form: PropTypes.bool,
    hideTiles: PropTypes.func,
    list: React.PropTypes.arrayOf(React.PropTypes.object),
    onHighScoreChange: PropTypes.func,
    onSaveClick: PropTypes.func,
    onTileClick: PropTypes.func,
    percentage: PropTypes.number,
    player: PropTypes.string,
    setStatusLoose: PropTypes.func,
    status: PropTypes.string,
    step: PropTypes.number,
    tiles: React.PropTypes.arrayOf(React.PropTypes.object)
};

const mapStateToProps = state => ({
    form: state.score.form,
    list: state.score.list,
    percentage: state.play.percentage || 0,
    player: state.score.player,
    status: state.play.status,
    step: state.play.step || 0,
    tiles: state.play.tiles || []
});

const mapDispatchToProps = dispatch => ({
    fetchTiles: () => {
        dispatch(fetchTiles());
        dispatch(enableScoreForm());
    },
    hideTiles: () => dispatch(hideTiles()),
    onHighScoreChange: event => dispatch(setHighScore(event)),
    onTileClick: index => dispatch(toggleTile(index)),
    onSaveClick: (step, percentage) => dispatch(saveHighScore(step, percentage)),
    setStatusLoose: () => dispatch(setStatus(STATUS_LOOSE))
});

export default connect(mapStateToProps, mapDispatchToProps)(GameScreen);
