import { assert } from 'chai';
import reducer, { setHighScore, saveHighScore, enableScoreForm,
    SET_HIGH_SCORE, SAVE_HIGH_SCORE, ENABLE_SCORE_FORM } from '../modules/score';

describe('score module', () => {
    describe('actions', () => {
        describe('setHighScore()', () => {
            it('should update SET_HIGH_SCORE action', () => {
                const action = { type: SET_HIGH_SCORE, player: 'hello' };
                assert.deepEqual(setHighScore({ target: { value: 'hello' } }), action);
            });
        });

        describe('saveHighScore()', () => {
            it('should update SAVE_HIGH_SCORE action', () => {
                const action = { type: SAVE_HIGH_SCORE, data: { step: 1, percentage: 20 } };
                assert.deepEqual(saveHighScore(1, 20), action);
            });
        });

        describe('enableScoreForm()', () => {
            it('should update ENABLE_SCORE_FORM action', () => {
                const action = { type: ENABLE_SCORE_FORM };
                assert.deepEqual(enableScoreForm(), action);
            });
        });
    });

    describe('reducers', () => {
        describe('SET_HIGH_SCORE', () => {
            it('should update the higher player state object', () => {
                const action = { type: SET_HIGH_SCORE, player: 'test' };
                const initialState = {
                    player: 'test'
                };
                const expectedState = {
                    player: 'test'
                };

                assert.deepEqual(reducer(initialState, action), expectedState);
            });
        });

        describe('SAVE_HIGH_SCORE', () => {
            it('should save the high score', () => {
                const action = { type: SAVE_HIGH_SCORE, data: { step: 2, percentage: 50 } };
                const initialState = {
                    form: true,
                    list: [],
                    player: 'test'
                };
                const expectedState = {
                    form: false,
                    list: [{ step: 2, player: 'test', percentage: 50 }],
                    player: ''
                };

                assert.deepEqual(reducer(initialState, action), expectedState);
            });

            it('should keep the list at 10 players', () => {
                const action = { type: SAVE_HIGH_SCORE, data: { step: 2, percentage: 20 } };
                const initialState = {
                    form: true,
                    list: [
                        { step: 0, player: '0' },
                        { step: 1, player: '1' },
                        { step: 3, player: '3' },
                        { step: 4, player: '4' },
                        { step: 5, player: '5' },
                        { step: 6, player: '6' },
                        { step: 7, player: '7' },
                        { step: 8, player: '8' },
                        { step: 9, player: '9' },
                        { step: 0, player: '0' },
                        { step: 0, player: '0' },
                        { step: 0, player: '0' }
                    ],
                    player: '2'
                };
                const expectedState = {
                    form: false,
                    list: [
                        { step: 9, player: '9' },
                        { step: 8, player: '8' },
                        { step: 7, player: '7' },
                        { step: 6, player: '6' },
                        { step: 5, player: '5' },
                        { step: 4, player: '4' },
                        { step: 3, player: '3' },
                        { step: 2, player: '2', percentage: 20 },
                        { step: 1, player: '1' },
                        { step: 0, player: '0' }
                    ],
                    player: ''
                };

                assert.deepEqual(reducer(initialState, action), expectedState);
                assert.equal(reducer(initialState, action).list.length, 10, 'Should have only 10 players');
            });
        });

        describe('ENABLE_SCORE_FORM', () => {
            it('should enable the form', () => {
                const action = { type: ENABLE_SCORE_FORM };
                const initialState = {
                    form: false
                };
                const expectedState = {
                    form: true
                };

                assert.deepEqual(reducer(initialState, action), expectedState);
            });
        });
    });
});
