import store from 'store';

export const SET_HIGH_SCORE = 'SET_HIGH_SCORE';
export const ENABLE_SCORE_FORM = 'ENABLE_SCORE_FORM';
export const SAVE_HIGH_SCORE = 'SAVE_HIGH_SCORE';

export function setHighScore(event) {
    return { type: SET_HIGH_SCORE, player: event.target.value };
}

export function saveHighScore(step, percentage) {
    return { type: SAVE_HIGH_SCORE, data: { step, percentage } };
}

export function enableScoreForm() {
    return { type: ENABLE_SCORE_FORM };
}

const initialState = {
    player: '',
    form: true,
    list: store.get('PathFindrScore') || []
};

export default function score(state = initialState, action) {
    switch (action.type) {

    case SET_HIGH_SCORE: {
        return { ...state, player: action.player };
    }

    case SAVE_HIGH_SCORE: {
        const list = Object.assign([], state.list);
        let form = state.form;
        list.push({
            player: state.player,
            step: action.data.step,
            percentage: action.data.percentage
        });

        list.sort((a, b) => b.step - a.step || b.percentage - a.percentage);
        form = false;

        const newList = list.slice(0, 10);
        store.set('PathFindrScore', newList);
        return { ...state, list: newList, form, player: '' };
    }

    case ENABLE_SCORE_FORM: {
        return { ...state, form: true };
    }

    default: {
        return state;
    }
    }
}
