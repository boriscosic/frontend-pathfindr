import Board from '../lib/board';

export const FETCH_TILES = 'FETCH_TILES';
export const HIDE_TILES = 'HIDE_TILES';
export const TOGGLE_TILE = 'TOGGLE_TILE';
export const SET_STATUS = 'SET_STATUS';
export const STATUS_END = 'STATUS_END';
export const STATUS_NEW = 'STATUS_NEW';
export const STATUS_LOOSE = 'STATUS_LOOSE';
export const STATUS_PLAYING = 'STATUS_PLAYING';
export const STATUS_WIN = 'STATUS_WIN';

export function fetchTiles() {
    return { type: FETCH_TILES };
}

export function toggleTile(tile) {
    return { type: TOGGLE_TILE, tile };
}

export function hideTiles(tile) {
    return { type: HIDE_TILES, tile };
}

export function setStatus(status) {
    return { type: SET_STATUS, status };
}

export default function play(state = {}, action) {
    switch (action.type) {

    case FETCH_TILES: {
        const tileCount = STARTING_TILES_PER_ROW;
        return { ...state,
            tiles: Board.generate(tileCount),
            status: STATUS_NEW,
            tileCount,
            step: 0,
            lastTile: null,
            percentage: 0
        };
    }

    case TOGGLE_TILE: {
        const tiles = Object.assign([], state.tiles);
        const percentage = parseInt((state.tiles.filter(e => e.played).length /
            state.tiles.filter(e => e.playable).length) * 100, 0);

        if (tiles.filter(t => t.visible).length > 0 || tiles[action.tile].played === true ||
            state.status === STATUS_END) {
            return state;
        }

        if (Board.isValid(action.tile, state.lastTile, state.tiles) && state.step < GAME_STEPS) {
            tiles[action.tile].played = true;

            if (tiles.filter(t => t.played).length === tiles.filter(t => t.playable).length) {
                if (state.step + 1 === GAME_STEPS) {
                    return { ...state, status: STATUS_WIN, percentage: 100 };
                }

                return { ...state,
                    step: state.step + 1,
                    tileCount: state.tileCount + 1,
                    tiles: Board.generate(state.tileCount + 1),
                    status: STATUS_NEW,
                    lastTile: null,
                    percentage: 0
                };
            }

            return { ...state,
                lastTile: action.tile,
                tiles,
                status: STATUS_PLAYING,
                percentage
            };
        }

        tiles[action.tile].error = true;
        return { ...state,
            status: STATUS_END,
            percentage,
            tiles
        };
    }

    case HIDE_TILES: {
        if (state.tiles.filter(t => t.visible).length > 0 &&
            state.status === STATUS_NEW &&
            state.tiles.filter(t => t.played).length === 0) {
            const tiles = state.tiles.map((tile) => {
                tile.visible = false; // eslint-disable-line
                return tile;
            });

            return { ...state, status: STATUS_PLAYING, tiles };
        }

        return state;
    }

    case SET_STATUS: {
        return { ...state, status: action.status };
    }

    default: {
        return state;
    }
    }
}
