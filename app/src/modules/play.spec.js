import { assert } from 'chai';
import reducer, { fetchTiles, toggleTile, setStatus, STATUS_LOOSE, FETCH_TILES,
    TOGGLE_TILE, STATUS_NEW, STATUS_END, SET_STATUS } from './play';
import Board from '../lib/Board';

describe('play module', () => {
    let tiles = null;

    describe('actions', () => {
        describe('fetchTiles()', () => {
            it('should create FETCH_TILES action', () => {
                const action = { type: FETCH_TILES };
                assert.deepEqual(fetchTiles(), action);
            });
        });

        describe('setStatus()', () => {
            it('should create SET_STATUS action', () => {
                const action = { type: SET_STATUS, status: STATUS_LOOSE };
                assert.deepEqual(setStatus(STATUS_LOOSE), action);
            });
        });

        describe('toggleTile()', () => {
            it('should create TOGGLE_TILE action', () => {
                const action = { type: TOGGLE_TILE, tile: 1 };
                assert.deepEqual(toggleTile(1), action);
            });
        });
    });

    describe('reducers', () => {
        describe('board lib', () => {
            it('should get a new tileset each time', () => {
                const tiles1 = Board.generate(9);
                tiles = Board.generate(9);
                assert.notEqual(tiles1, tiles);
            });
        });

        describe('FETCH_TILES', () => {
            it('should create tiles for the game', () => {
                const action = { type: FETCH_TILES };
                const tileCount = 2;
                const expectedState = {
                    lastTile: null,
                    tiles,
                    status: STATUS_NEW,
                    tileCount,
                    step: 0,
                    percentage: 0
                };
                const resultState = reducer({}, action);
                resultState.tiles = tiles;

                assert.deepEqual(resultState, expectedState);
            });
        });

        describe('TOGGLE_TILE', () => {
            it('should loose the game if wrong tile is clicked', () => {
                const action = { type: TOGGLE_TILE, tile: 1 };
                const initialState = {
                    lastTile: null,
                    tiles: [
                        { playable: true },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false }
                    ],
                    status: STATUS_NEW,
                    tileCount: 2,
                    step: 0
                };
                const expectedState = {
                    lastTile: null,
                    tiles: [
                        { playable: true },
                        { playable: false, error: true },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false }
                    ],
                    status: STATUS_END,
                    tileCount: 2,
                    step: 0,
                    percentage: 0
                };
                const resultState = reducer(initialState, action);
                assert.deepEqual(resultState, expectedState);
            });

            it('should win the game if right tile is clicked', () => {
                const action = { type: TOGGLE_TILE, tile: 0 };
                const initialState = {
                    lastTile: null,
                    tiles: [
                        { playable: true, played: false, index: 0, order: 0 },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false }
                    ],
                    status: STATUS_NEW,
                    tileCount: 2,
                    step: 0
                };

                const resultState = reducer(initialState, action);
                assert.notEqual(resultState.tileCount, initialState.tileCount, 'Should have more tiles');
                assert.equal(resultState.status, initialState.status, 'Should have new status');
                assert.notEqual(resultState.step, initialState.step, 'Should have a new step');
            });
        });

        describe('SET_STATUS', () => {
            it('should set status for the game', () => {
                const action = { type: SET_STATUS, status: STATUS_LOOSE };
                const expectedState = {
                    status: STATUS_LOOSE
                };
                const resultState = reducer({}, action);
                assert.deepEqual(resultState, expectedState);
            });
        });
    });
});
