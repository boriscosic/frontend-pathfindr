#!/bin/sh

STAGED_FILES=$(git diff --cached --name-only --diff-filter=ACM | grep ".jsx\{0,1\}$")

if [[ "$STAGED_FILES" = "" ]]; then
  exit 0
fi

echo "\nValidating JavaScript:\n"

npm run lint

if [[ "$?" -ne 0 ]]; then
  echo "\033[41mCOMMIT FAILED:\033[0m Your commit contains files that should pass ESLint but do not. Please fix the ESLint errors and try again.\n"
  exit 1
else
  echo "\033[42mCOMMIT SUCCEEDED\033[0m\n"
fi

echo "\nRunning tests:\n"

npm test

if [[ "$?" -ne 0 ]]; then
  echo "\033[41mCOMMIT FAILED:\033[0m Not all tests passed.\n"
  exit 1
else
  echo "\033[42mCOMMIT SUCCEEDED\033[0m\n"
fi

exit $?
